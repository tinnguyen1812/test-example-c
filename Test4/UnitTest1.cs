﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace Test4
{
    
    public class UnitTest1
    {
        [Test]
        public void TestMethod1 ()
        {
            IWebDriver _driver = new ChromeDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20); 
            _driver.Manage().Window.Maximize();
            _driver.Navigate().GoToUrl("https://uat-www.jeanswest.com.au/en-au/auth?from=/en-au");
            _driver.FindElement(By.Id("login")).SendKeys("dicky_cheung22@yahoo.com");
            _driver.FindElement(By.Id("password")).SendKeys("456789");
            _driver.FindElement(By.XPath("//input[@value='Sign In']")).Click();
            _driver.FindElement(By.CssSelector(".header-bar-btn.sign-btn")).Click();
            Thread.Sleep(2000);
            String profile_Name = _driver.FindElement(By.CssSelector(".sign-in.dis-none .header-norhead.ft-red")).Text;
            Console.WriteLine("profile_Name: " + profile_Name);
            Assert.AreEqual(profile_Name, "tin nguyen");
            _driver.Quit();
        }
    }
}
