﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Test4.Manager;

namespace Test4.Hooks
{
    [Binding]
    public sealed class Hook
    {
        private readonly IObjectContainer objectContainer;

        public Hook (IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }


        [BeforeScenario]
        public void InitializeWebDriver ()
        {
            var webDriver = new ChromeDriver();
            objectContainer.RegisterInstanceAs<IWebDriver>(webDriver);
            //objectContainer.RegisterInstanceAs(new PageObjectManager());
        }
    }
}
