﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test4.PageObject;

namespace Test4.Manager
{
    public class PageObjectManager
    {
        private IWebDriver driver;
        private LoginPage loginPage;
        private HomePage homePage;

        public PageObjectManager (IWebDriver driver)
        {
            this.driver = driver;
        }

        public HomePage GetHomePage ()
        {
            return (homePage == null) ? homePage = new HomePage(driver) : homePage;
        }

        public LoginPage GetLoginPage ()
        {
            return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
        }

    }
}
