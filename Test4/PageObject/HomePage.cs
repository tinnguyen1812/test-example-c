﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test4.PageObject
{
    public class HomePage
    {
        IWebDriver driver;

        public HomePage (IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".header-bar-btn.sign-btn")]
        private IWebElement Icon_User;

        [FindsBy(How = How.CssSelector, Using = ".sign-in.dis-none .header-norhead.ft-red")]
        private IWebElement Txt_ProfileName;


        
        public void CheckLoginSuccessfully ()
        {
            Icon_User.Click();
            String name = Txt_ProfileName.Text;
            Assert.AreEqual(name, "tin nguyen");
        }
    }
}
