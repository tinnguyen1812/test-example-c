﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Test4.PageObject
{
    public class LoginPage
    {
        IWebDriver driver;

        public LoginPage (IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "login")]
        private IWebElement Txtbx_Email { get; set; }

        [FindsBy(How = How.Id, Using = "password")]
        private IWebElement Txtbx_Password { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value='Sign In']")]
        private IWebElement Btn_SignIn { get; set; }

        public void Navigate_LoginPage ()
        {
            driver.Navigate().GoToUrl("https://uat-www.jeanswest.com.au/en-au/auth?from=/en-au");
        }

        public void Login ()
        {
            Txtbx_Email.SendKeys("dicky_Cheung22@yahoo.com");
            Txtbx_Password.SendKeys("456789");
        }

        public void ClickSignInButton()
        {
            Btn_SignIn.Click();
        }

        public void QuitBrowser ()
        {
            driver.Quit();
        }
    }
}
