﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using Test4.Manager;
using Test4.PageObject;

namespace Test4.StepsDefinition
{
    [Binding]
    public class LoginPageSteps
    {
        LoginPage loginPage;
       // HomePage homePage;
        IWebDriver driver;
        PageObjectManager pageObjectManager;

        public LoginPageSteps (IWebDriver driver, PageObjectManager pageObjectManager)
        {
            //loginPage = new LoginPage(driver);
            this.driver = driver;
            this.pageObjectManager = pageObjectManager;
        }

        [Given(@"I am on Login Page")]
        public void GivenIAmOnLoginPage()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            driver.Manage().Window.Maximize();
            //pageObjectManager = new PageObjectManager(driver);
            loginPage = pageObjectManager.GetLoginPage();
            loginPage.Navigate_LoginPage();
        }
        
        [When(@"I input username and password")]
        public void WhenIInputUsernameAndPassword()
        {
            //loginPage = new LoginPage(driver);
            loginPage.Login();
        }
        
        [When(@"I click Sign In button")]
        public void WhenIClickSignInButton()
        {
            //loginPage = new LoginPage(driver);
            loginPage.ClickSignInButton();
        }
        
        
    }
}
