﻿using BoDi;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;
using Test4.Manager;
using Test4.PageObject;

namespace Test4.StepsDefinition
{
    [Binding]
    public class HomePageSteps
    {
        IWebDriver driver;
        HomePage homePage;
        private readonly IObjectContainer objectContainer;
        private PageObjectManager pageObjectManager;
        public HomePageSteps (PageObjectManager pageObjectManager)
        {
            this.pageObjectManager = pageObjectManager;
        }

        [Then(@"I will be redirected to Home Page and logged in successfully")]
        public void ThenIWillBeRedirectedToHomePageAndLoggedInSuccessfully()
        {
            //homePage = new HomePage(driver);
            //objectContainer.IsRegistered<IWebDriver>();
            //pageObjectManager = new PageObjectManager(driver);
            homePage = pageObjectManager.GetHomePage();
            homePage.CheckLoginSuccessfully();
        }
    }
}
